.. _project_index:

Design a liquid hydrogen powered aircraft
=========================================

.. warning::

   To update the HTML rendering of the project,
   please move to the root of the directory **project**
   and compile the project with the command line **make html**.

This project aims to design a liquid hydrogen powered aircraft (LH2PAC)
from several Python libraries:
`MARILib <https://github.com/marilib/MARILib_obj>`_ for aircraft modeling,
`scikit-learn <https://github.com/scikit-learn/scikit-learn>`_ for surrogate modeling,
`OpenTURNS <https://github.com/openturns/openturns>`_ for uncertainty quantification,
`GEMSEO <https://gitlab.com/gemseo/dev/gemseo>`_ for process orchestration and optimization.


.. toctree::
   :maxdepth: 2

   problem
   deliverables
   Use Case <use_case>
   Examples <examples/index>
   Results <results/index>
   report/index

.. toctree::
   :maxdepth: 2
   :caption: Appendices:

   reST introduction <restructuredtext>
